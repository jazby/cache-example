# README #

This is a PHP library for testing purposes.

It implements [Psr\Cache](http://www.php-fig.org/psr/psr-6/). 
This package has intentionally some implementation faults and 
it does not strictly follow the documentation of the interface. 

### Requirements ###

* PHP 7
* Composer

### Installation and run ###

* composer install
* ./bin/codecept run

### What is my task? ###

* Read this readme.
* Examine the difficulty of the task
and send us a deadline you will have your solution ready.
* Fork this repository.
* Find implementation faults and bugs and fix them.
* Find differences between this package behaviour 
and interface documentation and fix them.  
* Modify the library so the cache storage can be easily
switched during runtime.
* Implement filesystem cache storage.
* Create pull request with you changes and describe them in the comment.

### Hints ###
* [TDD](https://en.wikipedia.org/wiki/Test-driven_development)
* [DI](https://en.wikipedia.org/wiki/Dependency_injection)
* [OOP patterns](http://www.oodesign.com/)




