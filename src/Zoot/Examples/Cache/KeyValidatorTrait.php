<?php
declare(strict_types=1);


namespace Zoot\Examples\Cache;


use Zoot\Examples\Cache\Exception\InvalidArgumentException;

/**
 * Trait KeyValidatorTrait
 *
 * @package Zoot\Examples\Cache
 */
trait KeyValidatorTrait
{
    /**
     * @param $key
     *
     * @return bool
     */
    protected function isKeyValid($key) : bool
    {
        return !preg_match('#[/\\\@:]#', $key);
    }

    /**
     * @param $key
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    protected function validateKey($key) : bool
    {
        if(!$this->isKeyValid($key)) {
            throw new InvalidArgumentException(sprintf('Key "%s" is not valid.', $key));
        }

        return true;
    }
}