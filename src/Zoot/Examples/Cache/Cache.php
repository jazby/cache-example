<?php
declare(strict_types=1);

namespace Zoot\Examples\Cache;

use Psr\Cache\{
    CacheItemInterface, CacheItemPoolInterface
};

/**
 * Class Cache
 *
 * @package Zoot\Examples\Cache
 */
class Cache implements CacheItemPoolInterface
{
    use KeyValidatorTrait;

    /**
     * @var Cache
     */
    protected static $instance = null;

    /**
     * @var array
     */
    protected $pool = [];

    /**
     * Cache constructor.
     */
    protected final function __construct()
    {
        $this->pool = [];
    }

    /**
     * @return Cache
     */
    public static final function getInstance(): Cache
    {
        if (is_null(static::$instance)) {
            $class            = get_called_class();
            static::$instance = new $class;
        }

        return static::$instance;
    }

    /**
     * @param array $keys
     *
     * @return array
     */
    public function getItems(array $keys = []): array
    {
        $items = [];
        foreach ($keys as $key) {
            $items[$key] = $this->getItem($key);
        }

        return $items;
    }

    /**
     * @param string $key
     *
     * @return CacheItemInterface
     */
    public function getItem($key): CacheItemInterface
    {
        $this->validateKey($key);

        return $this->pool[$key] ?? new Item();
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasItem($key): bool
    {
        $item = $this->getItem($key);

        return $item->isHit();
    }

    /**
     * @return bool
     */
    public function clear(): bool
    {
        $this->pool = [];

        return true;
    }

    /**
     * @param string[] $keys
     *
     * @return bool
     */
    public function deleteItems(array $keys): bool
    {
        foreach ($keys as $key) {
            $this->deleteItem($key);
        }

        return true;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function deleteItem($key): bool
    {
        $item = $this->getItem($key);

        if ($item->isHit()) {
            unset($this->pool[$key]);
        }

        return true;
    }

    /**
     * @param CacheItemInterface $item
     *
     * @return bool
     */
    public function saveDeferred(CacheItemInterface $item): bool
    {
        return $this->save($item);
    }

    /**
     * @param CacheItemInterface $item
     *
     * @return bool
     */
    public function save(CacheItemInterface $item): bool
    {
        $this->pool[$item->getKey()] = $item;

        return true;
    }

    /**
     * @return bool
     */
    public function commit(): bool
    {
        return true;
    }
}