<?php
declare(strict_types=1);


namespace Zoot\Examples\Cache\Exception;


/**
 * Class CacheException
 *
 * @package Zoot\Examples\Cache\Exception
 */
class CacheException extends \Exception implements \Psr\Cache\CacheException
{

}