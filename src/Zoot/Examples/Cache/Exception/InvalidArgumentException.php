<?php
declare(strict_types=1);


namespace Zoot\Examples\Cache\Exception;


/**
 * Class InvalidArgumentException
 *
 * @package Zoot\Examples\Cache\Exception
 */
class InvalidArgumentException extends CacheException implements \Psr\Cache\InvalidArgumentException
{

}