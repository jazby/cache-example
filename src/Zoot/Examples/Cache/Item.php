<?php
declare(strict_types=1);

namespace Zoot\Examples\Cache;

use Psr\Cache\CacheItemInterface;

/**
 * Class Item
 *
 * @package Zoot\Examples\Cache
 */
class Item implements CacheItemInterface
{
    use KeyValidatorTrait;

    /**
     * Default item expiration in seconds
     */
    const DEFAULT_TTL = 3600;

    /**
     * @var string
     */
    protected $key = null;
    /**
     * @var mixed
     */
    protected $value = null;
    /**
     * @var bool
     */
    protected $hit = false;
    /**
     * @var \DateTimeInterface
     */
    protected $expiresAt = null;

    /**
     * Item constructor.
     *
     * @param string    $key
     * @param mixed     $value
     * @param \DateTime $expiresAt
     */
    public function __construct(string $key = null, $value = null, \DateTimeInterface $expiresAt = null)
    {
        if (!is_null($key)) {
            $this->validateKey($key);
        }

        $this->key       = $key;
        $this->value     = $value;
        $this->expiresAt = $expiresAt ?? $this->getDefaultExpiresAt();
    }

    protected function getDefaultExpiresAt()
    {
        return new \DateTime(sprintf('+%d seconds', self::DEFAULT_TTL));
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return mixed|null
     */
    public function get()
    {
        $value = $this->value;
        if (!$this->isHit()) {
            $value = null;
        }

        return $value;
    }

    public function isHit(): bool
    {
        return $this->expiresAt >= new \DateTime();
    }

    /**
     * @param mixed $value
     *
     * @return Item
     */
    public function set($value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @param \DateTimeInterface|null $expiration
     *
     * @return Item
     */
    public function expiresAt($expiration): self
    {
        if (!($expiration instanceof \DateTimeInterface)) {
            $expiration = $this->getDefaultExpiresAt();
        }

        $this->expiresAt = $expiration;

        return $this;
    }

    /**
     * @param int|null $time
     *
     * @return Item
     */
    public function expiresAfter($time): self
    {
        if (is_null($time)) {
            $expiresAt = $this->getDefaultExpiresAt();
        }

        $this->expiresAt = $expiresAt;

        return $this;
    }
}