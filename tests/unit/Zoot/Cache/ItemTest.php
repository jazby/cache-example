<?php
use Zoot\Examples\Cache\Item;

class ItemTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testItemCanBeInstantiated()
    {
        $item = new Item();

        $this->assertNull($item->get());
        $this->assertNull($item->getKey());

        $item = new Item('item key', 'item value');
        $this->assertEquals('item key', $item->getKey());
        $this->assertEquals('item value', $item->get());
        $this->assertEquals('item value', $item->get());
    }

    public function testItemExpiration()
    {
        $item = new Item('item key', 'item value', new \DateTime('2029-01-01 00:00:00'));
        $this->assertEquals(true, $item->isHit());
        $this->assertEquals('item value', $item->get());

        $item = new Item('item key', 'item value', new \DateTime('1989-01-01 00:00:00'));
        $this->assertEquals(false, $item->isHit());
        $this->assertNull($item->get());
    }

    public function testKeyValidation()
    {
        $item = new Item('this is a valid key', 'value');
        $this->assertEquals('this is a valid key', $item->getKey());

        $this->expectException(\Zoot\Examples\Cache\Exception\InvalidArgumentException::class);
        $item = new Item('this should be invalid key @', 'value');
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }
}