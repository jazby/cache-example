<?php
use Zoot\Examples\Cache\Cache;
use Zoot\Examples\Cache\Item;

class CacheTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testSingletonPattern()
    {
        $instance = Cache::getInstance();
        $this->assertInstanceOf(Cache::class, $instance);

        $secondInstance = Cache::getInstance();
        $this->assertEquals($instance, $secondInstance);
    }

    public function testItemCanBeSavedAndRestored()
    {
        $instance = Cache::getInstance();

        $this->assertTrue($instance->save(new Item('item01', 'item01 value')));

        $item = $instance->getItem('item01');
        $this->assertInstanceOf(Item::class, $item);
        $this->assertEquals('item01 value', $item->get());
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }
}